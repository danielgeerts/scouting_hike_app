﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class ShowAllAnswers : MonoBehaviour {

    public GameObject prefabToLoad;
    public GameObject placeToPlaceButton;

    private List<Answer_properties> allRightAnswers;
    private List<GameObject> allButtons;

    //public Text AmoutWrongText;
    //private int amoutOfWrong = 0;

    private GameObject answerToShow;
    public GameObject answerPanel;

    // Use this for initialization
    void Start () {
        allRightAnswers = new List<Answer_properties>();
        allButtons = new List<GameObject>();
        //AmoutWrongText.text = "Aantal fout ingevoerd: " + amoutOfWrong;

        answerPanel.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        for (int i = 0; i < this.gameObject.GetComponent<Answer_Checker>().getAllAnswers().Count; i++) {
            if (this.gameObject.GetComponent<Answer_Checker>().getAllAnswers()[i].getIsFound() &&
                !allRightAnswers.Contains(this.gameObject.GetComponent<Answer_Checker>().getAllAnswers()[i])) {
                allRightAnswers.Add(this.gameObject.GetComponent<Answer_Checker>().getAllAnswers()[i]);

                GameObject newbtn = Instantiate(prefabToLoad, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                newbtn.GetComponentInChildren<Text>().text = allRightAnswers[allRightAnswers.Count -1].answer;

                newbtn.gameObject.transform.SetParent(placeToPlaceButton.transform);
                allButtons.Add(newbtn);
                placeToPlaceButton.GetComponent<RectTransform>().sizeDelta = new Vector2(0, placeToPlaceButton.GetComponent<RectTransform>().sizeDelta.y + 150);
            }
        }

        for (int j = 0; j < allButtons.Count; j++) {
            allButtons[j].transform.position = new Vector3(placeToPlaceButton.transform.position.x + 316.5f, placeToPlaceButton.transform.position.y - ((j * 150)) - 125, placeToPlaceButton.transform.position.z);
        }
    }

    public void addOneToAmoutOfWrong() {
        //amoutOfWrong++;
        //AmoutWrongText.text = "Aantal fout ingevoerd: " + amoutOfWrong;
    }

    public void ButtonOnClick() {
        for (int i = 0; i < allButtons.Count; i++) {
            for (int j = 0; j < allRightAnswers.Count; j++) {
                string currentButtonCheck = allButtons[i].GetComponentInChildren<Text>().text;
                string CurrentButtonClicked = EventSystem.current.currentSelectedGameObject.GetComponentInChildren<Text>().text;
                if (currentButtonCheck.Equals(CurrentButtonClicked)) {
                    if (currentButtonCheck.Equals(allRightAnswers[j].answer)) {
                        answerToShow = Instantiate(allRightAnswers[j].answerToHide, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                        answerToShow.SetActive(true);
                        answerToShow.gameObject.transform.SetParent(answerPanel.transform);
                        answerToShow.transform.localPosition = new Vector3(0, 0, 0);
                        answerPanel.SetActive(true);
                    }
                }
            }
        }
    }

    public void ClosePanelWithAnswer() {
        Destroy(answerToShow.gameObject);
        answerPanel.SetActive(false);
    }
}
