﻿using UnityEngine;
using System.Collections;

public class Answer_properties : MonoBehaviour {

    public string answer = "0000";

    public GameObject answerToHide;
    private bool isFound = false;

    // Use this for initialization
    void Start () {
        
    }

    // Update is called once per frame
    void Update () {
	
	}

    public void setIsFound(bool value) {
        isFound = value;
    }

    public bool getIsFound() {
        return isFound;
    }
}
