﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Answer_Checker : MonoBehaviour {

    public Canvas InputCodeCanvas;
    public Canvas AllAnswersCanvas;

    public string masterCode = "gDg7";

    public InputField inputCodeField;
    public GameObject Wrong_Code;
    public GameObject Master_Code;
    public List<GameObject> All_Codes;

    private bool showWrong = false;

    private List<Answer_properties> all_answers;

    // Use this for initialization
    void Start () {
        LoadInputCodeCanvas();

        all_answers = new List<Answer_properties>();

        for (int i = 0; i < All_Codes.Count; i++) {
            all_answers.Add(All_Codes[i].GetComponent<Answer_properties>());
        }

        for (int i = 0; i < all_answers.Count; i++) {
            all_answers[i].answerToHide.SetActive(false);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (inputCodeField.isFocused) {
            for (int j = 0; j < all_answers.Count; j++) {
                all_answers[j].answerToHide.SetActive (false);
            }

            Wrong_Code.GetComponent<Answer_properties> ().answerToHide.SetActive (false);
        }
    }

    public void CheckForAnwer() {
        if (inputCodeField.text.Length == 4) {
            for (int i = 0; i < all_answers.Count; i++) {
                all_answers[i].answerToHide.SetActive (false);
            }

            if (inputCodeField.text.Equals (masterCode)) {
                for (int j = 0; j < all_answers.Count; j++) {
                    //all_answers[j].answerToHide.SetActive (true);
                    Master_Code.GetComponent<Answer_properties> ().answerToHide.SetActive (true);
                    all_answers[j].setIsFound (true);
                }
                return;
            }

            for (int i = 0; i < All_Codes.Count; i++) {
                if (inputCodeField.text.Equals (All_Codes[i].GetComponent<Answer_properties> ().answer)) {
                    for (int j = 0; j < all_answers.Count; j++) {
                        if (all_answers[j].gameObject.Equals (All_Codes[i])) {
                            all_answers[j].answerToHide.SetActive (true);
                            all_answers[j].setIsFound (true);
                            showWrong = false;
                            return;
                        }
                        showWrong = true;
                    }
                    return;
                } else {
                    showWrong = true;
                }
            }
            if (showWrong) {
                Wrong_Code.GetComponent<Answer_properties> ().answerToHide.SetActive (true);
                this.gameObject.GetComponent<ShowAllAnswers> ().addOneToAmoutOfWrong ();
                showWrong = false;
            }
        } else {
            Wrong_Code.GetComponent<Answer_properties> ().answerToHide.SetActive (true);
            this.gameObject.GetComponent<ShowAllAnswers> ().addOneToAmoutOfWrong ();
        }
    }

    public void LoadInputCodeCanvas() {
        InputCodeCanvas.enabled = true;
        AllAnswersCanvas.enabled = false;
    }

    public void LoadAllAnswersCanvas() {
        InputCodeCanvas.enabled = false;
        AllAnswersCanvas.enabled = true;
    }

    public List<Answer_properties> getAllAnswers() {
        return all_answers;
    }

    public void unlockEverything() {

    }
}
